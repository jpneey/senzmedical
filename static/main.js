$(function() {

// Function Variables

  var elements;
  var windowHeight;
  var request;


// Fade in on Scroll

  function init() {
    elements = document.querySelectorAll('.hidden');
    windowHeight = window.innerHeight;
  }


  function checkPosition() {
    for (var i = 0; i < elements.length; i++) {
      var element = elements[i];
      var positionFromTop = elements[i].getBoundingClientRect().top;

      if (positionFromTop - windowHeight <= 0) {
        element.classList.add('fade-in-bottom');
        element.classList.remove('hidden');
      }
    }
  }

  window.addEventListener('scroll', checkPosition);
  window.addEventListener('resize', init);

  
// Ajax Forms

  // Bind to the submit event of our form
  $("[id=ajax-form]").submit(function(event) {

    event.preventDefault();
      
    if (request) {
          request.abort();
    }
    
    var $form = $(this);
    var $url = $(this).attr('action');
    var $inputs = $form.find("input, select, button, textarea");
    var serializedData = $form.serialize();
    
    $inputs.prop("disabled", true);
    window.onbeforeunload = function() {
      return 'Are you sure you want to navigate away from this page?';
    };
    $('.loader').fadeIn(500);
    request = $.ajax({
          url: $url,
          type: "post",
          data: serializedData
    });
    request.done(function (response, textStatus, jqXHR){
      
      $('.loader').fadeOut(500);
      
      window.onbeforeunload = null;
      M.toast({html: response});
      $inputs.val('');
    });
      
    request.fail(function (jqXHR, textStatus, errorThrown){
    
      console.error(
        "The following error occurred: "+
        textStatus, errorThrown
      );
      
      window.onbeforeunload = null;
    
    });

    request.always(function () {
    
      $inputs.prop("disabled", false);
      window.onbeforeunload = null;
    
    });

  });



// Materialize and Function Initializations 


  init();
  checkPosition();
  $('.sidenav').sidenav();
  $('.parallax').parallax();
  $('.carousel').carousel(); 

  $('.carousel.carousel-slider').carousel({
    fullWidth: true,
    noWrap: true,
    indicators: true,
    dist: -100
  });

  
  window.onscroll = function() {myFunction()};

  var header = document.getElementById("myHeader");
  var sticky = header.offsetTop;
  

  function myFunction() {
    if (window.pageYOffset > sticky) {
      header.classList.add("sticky");
      $(".brand").css({
        'color' : '#2d2d2d'
      });
    } else {
      header.classList.remove("sticky");
      $(".brand").css({
        'color' : 'white'
      })
    }
  }

  function r_size() {
    id="content_a"
    let s = $("#content_a").outerHeight();
    $("#content_b").css({
      'max-height' : s,
      'min-height' : s
    })
  }
  r_size();
})