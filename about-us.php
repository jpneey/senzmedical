<?php 
$include = true;
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>About Us - Senz Medical</title>
	<meta name="author" content="John Paul Burato" />
	<meta name="description" content="" />
	<meta name="keywords"  content=""/>
    <?php require_once "component/head.php"; ?>
</head>
<body>
	
    <?php require_once "component/loader.php"; ?>
    <?php require_once "component/navbar.php"; ?>
    <div class="full-width-banner">
    </div>
    <div class="section">
        <div class="container">
            <div class="container">
                <div class="row">
                    
                    <div class="col s12 m6">
                        <p class="senz-head">Lorem Ipsumis</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde quo repellat numquam est doloribus? Obcaecati velit nulla qui inventore dignissimos facilis laboriosam rerum doloribus aperiam odit! Impedit autem ducimus commodi!</p>
                        <p>Consectetur adipisicing elit. Unde quo repellat numquam est  Lorem ipsum dolor sit amet, consectetur adipisicing elit.<br>Unde quo repellat numquam est doloribus? rem ipsum dolor sit amet, consectetur adipisicing elit. Unde quo repellat numquam est doloribus? Obcaecati velit nulla qui inventore dignissimos facilis laboriosam rerum doloribus aperiam odit! Impedit autem ducimus commodi!</p>                    
                    </div>

                    <div class="col s12 m6">
                        <br><br>
                        <p>Consectetur adipisicing elit. Unde quo repellat numquam est  Lorem ipsum dolor sit amet, consectetur adipisicing elit.<br>Unde quo repellat numquam est doloribus? rem ipsum dolor sit amet, consectetur adipisicing elit. Unde quo repellat numquam est doloribus? Obcaecati velit nulla qui inventore dignissimos facilis laboriosam rerum doloribus aperiam odit! Impedit autem ducimus commodi!</p>                    
                        <a class="waves-effect waves-light btn-large senz-btn z-depth-0">read more</a>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m6">
                <div>

                </div>
            </div>
            <div class="col s12 m6">a</div>
        </div>
    </div>
    <?php require_once "component/footer.php"; ?>
</body>
</html>