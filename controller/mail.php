<?php 
sleep(2);
$mode = '';
if(!isset($_GET["mail"]) || $_GET["mail"] == '') {
    echo 'forbidden';
    die;
}
else {
    $mode = $_GET["mail"];
}

switch ($mode) {
    case 'newsletter':
        $required = array('cf-name', 'cf-email');

        $error = false;
        foreach($required as $field) {
            if (empty($_POST[$field])) {
                $error = true;
            }
        }
        if ($error) {
            
            echo "Message not sent. Please fill up the form with valid inputs";
            die;
        }
        require "template/newsletter.php";
        break;
    case 'contactus':
        $required = array('cf-name', 'cf-email', 'cf-location', 'cf-message');

        $error = false;
        foreach($required as $field) {
            if (empty($_POST[$field])) {
                $error = true;
            }
        }
        if ($error) {
            
            echo "Message not sent. Please fill up the form with valid inputs";
            die;
        }

        require "template/contactus.php";
        break;
    default:
        echo 'forbidden';
        die;
        break;
}

?>