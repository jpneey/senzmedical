<?php 
$include = true;
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Senz Medical</title>
	<meta name="author" content="John Paul Burato" />
	<meta name="description" content="" />
	<meta name="keywords"  content=""/>
    <?php require_once "component/head.php"; ?>
</head>
<body>
	
    <?php require_once "component/loader.php"; ?>
    <?php require_once "component/navbar.php"; ?>
    
    <?php require_once "component/banner.php"; ?>
	<div class="space"></div>

	<div class="container">
		<div class="row hidden">
			<div class="col s12 un-pad">
				<div class="col s12 m6 un-pad">
					<div class="card-img-wrapper init-img">
						<div class="card-img">
							<img src="assets/beta.png" class="img100" />
						</div>
					</div>
				</div>
				<div class="col s12 m6">
					<br><br>
					<span class="flow-text senz-head">The Leading Manufacturer<br>of lorem ipsum products!</span>
					<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat dignissimos excepturi praesentium eaque tempora, eligendi a mollitia nesciunt. Quo ipsam, id alias aut illo accusamus expedita debitis officiis veritatis quos?</p>
					<p>Quo ipsam, id alias aut illo accusamus expedita debitis officiis veritatis quos? Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat dignissimos excepturi praesentium eaque tempora, eligendi a mollitia nesciunt.</p>
					<a class="waves-effect waves-light btn-large senz-btn z-depth-0">read more</a>
				</div>
			</div>
		</div>
	</div>

	<div class="full-width bg-gray hidden">
		<div class="container">
			<div class="row">
				<div class="col s12 offset-m1 m5 right-align">
					<br><br>
					<span class="flow-text senz-head">Latest Manufacturing<br>Technology.</span>
					<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat dignissimos excepturi praesentium eaque tempora, eligendi a mollitia nesciunt. Quo ipsam, id alias aut illo accusamus expedita debitis officiis veritatis quos?</p>
					<p>Quo ipsam, id alias aut illo accusamus expedita debitis officiis veritatis quos? Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat dignissimos excepturi praesentium eaque tempora, eligendi a mollitia nesciunt.</p>
					<a class="waves-effect waves-light btn-large senz-btn z-depth-0">read more</a>
				</div>
				
				<div class="col offset-m1 s12 m5" id="pc">
					<div class="senz-card">
						<img src="assets/bag.png" class="senz-product-card" />
					</div>
				</diV>
			</div>
		</div>
	</div>

	<div class="container con-fix-misc">
		<div class="row hidden">
			<div class="col s12 un-pad">
				
				<div class="col s12 m6 un-pad vertical-align" id="content_a">
					<div class="card-img-wrapper full-deg full-card-wrapper">
					<br>
					<p class="flow-text senz-head">Products Crafted from<br>high quality Materials</p>				
					<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat dignissimos excepturi praesentium eaque tempora, eligendi a mollitia nesciunt. Quo ipsam, id alias aut illo accusamus expedita debitis officiis veritatis quos?</p>
					<a class="waves-effect waves-light btn-large senz-btn z-depth-0">read more</a>
					</div>
				</div>

				<div class="col s12 m6 un-pad">
					<div class="card-img-wrapper deg-deg"  id="content_b">
						<div class="card-img-label right-align">
							<p class="bg-img-label">read more lorem <br>dotoret as conectitur dore mi<br>id alias aut illo accusamus expedita debitis officiis veritatis quos?</p>
							<p class="sm-img-label right"  id="pc">loprem</p>
						</div>
						<div class="card-img-left">
							<img src="assets/mask.png" class="img90" />
						</div>
					</div>
				</div>


			</div>
		</div>
		<div class="row clear form-grid">
			<div class="hidden">
				<form id="ajax-form" action="controller/mail.php?mail=newsletter" method="POST">
					<p class="flow-text senz-head">Get a sneak peak</p>
					<p>Sign up to see what we've been working on.<br>Subscribe to our newsletter!</p>
					<input placeholder="Name" type="text" name="cf-name" value="" required>
					<input placeholder="Email address" type="email" name="cf-email" value="" required>
					<button class="waves-effect waves-light btn-large senz-btn z-depth-0">Subscribe</button>
				</form>
				<a class="btn-floating floating-count-odd z-depth-3"><i class="small material-icons">email</i></a>
			</div>

			<div class="hidden" id="contactus">
				<form id="ajax-form" action="controller/mail.php?mail=contactus" method="POST">
					<p class="flow-text senz-head">Reach Out to Us</p>
					<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Unde aut, recusandae quia eius voluptatem officiis sint reprehenderit pariatur libero voluptatum, quisquam nemo. Nisi ad error nihil nemo eius sunt explicabo.</p>
					<input placeholder="Name" name="cf-name" type="text"  value="" required>
					<input placeholder="Email address" name="cf-email" type="email" value="" required>
					<input placeholder="Location" type="text" name="cf-location" value="" required>
					<div class="flex">
						<textarea placeholder="Message" name="cf-message" rows="5" required></textarea>
					</div>
					<button type="submit" class="senz-btn z-depth-0 waves-effect waves-light btn"><i class="material-icons right">chevron_right</i>Send</button>
					<a href="mailto:email@sample.me" class="waves-effect waves-light btn-large senz-btn z-depth-0" id="pc"><i class="material-icons right">chevron_right</i>Manual Email</a>
				</form>
				<a class="btn-floating floating-count-even z-depth-3"><i class="small material-icons">drafts</i></a>
			</div>



		</div>
	</div>
	
	<div class="full-width hidden bg-gray">
		<div class="container">
			<div class="row hidden">

				<div class="col s12 m12">
					<p class="flow-text senz-head center-align">Looking for distributors ?<br>
					<span class="small-text center-align">We got you covered! See our trusted distributors:</span>
					</p>
				</div>

				<div class="col s12 m4">
					<div class="distributor-card">
						<p class="distributor-head dist-margin">Lorem Ipsum Company</p>
						<p class="small-text dist-margin">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dotor sit amet</p>
						<p class="distributor-footer no-margin">view details</p>
					</div>
				</div>

				<div class="col s12 m4">
					<div class="distributor-card">
						<p class="distributor-head dist-margin">Lorem Ipsum Company</p>
						<p class="small-text dist-margin">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dotor sit amet</p>
						<p class="distributor-footer no-margin">view details</p>
					</div>
				</div>

				<div class="col s12 m4">
					<div class="distributor-card">
						<p class="distributor-head dist-margin">Lorem Ipsum Company</p>
						<p class="small-text dist-margin">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dotor sit amet</p>
						<p class="distributor-footer no-margin">view details</p>
					</div>
				</div>
				
				<div class="col s12 m4">
					<div class="distributor-card">
						<p class="distributor-head dist-margin">Lorem Ipsum Company</p>
						<p class="small-text dist-margin">center-align</p>
						<p class="distributor-footer no-margin">view details</p>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div>
		<div class="container">
			<div class="row center-align">
				<h1 class=""><i class="medium material-icons">format_quote</i></h1>
				<div class="carousel carousel-slider testimonial-slider">
					<div class="carousel-item">
						<p><i>lorem ipsum dotor sit amet. center-aligncenter-align.<br>dotor sit amet lorem ipsum dotor sit amet. center-aligncenter-align<br>dotor sit amet lorem ipsum dotor sit<br><br>-Random Name</i></p>
					</div>
					<div class="carousel-item">
						<p><i>lorem ipsum dotor sit amet. center-aligncenter-align.<br>dotor sit amet lorem ipsum dotor sit amet. center-aligncenter-align<br>dotor sit amet lorem ipsum dotor sit<br><br>-Random Name</i></p>
					</div>
					<div class="carousel-item">
						<p><i>lorem ipsum dotor sit amet. center-aligncenter-align.<br>dotor sit amet lorem ipsum dotor sit amet. center-aligncenter-align<br>dotor sit amet lorem ipsum dotor sit<br><br>-Random Name</i></p>
					</div>
				</div>
			</div>
		</div>
	
	</div>
    <?php require_once "component/footer.php"; ?>

</body>
</html>