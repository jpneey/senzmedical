<?php if(!$include) { 
    die;
} ?>

<footer>
  <div>
    <div class="row container" id="footer-main-section">
      <div class="col s12 m12 l4 footer-section footer-header-section">
        <div class="footer-section-title">SENZ MEDICAL</div>
        <p>
        Our mission is to build the world’s most simple and reliable group email platform — we make group email available to everyone who needs it.
        </p>
        <div class="spacer hide-on-med-and-up"></div>
        <div class="spacer hide-on-med-and-up"></div>
      </div>
      <div class="col s12 m12 l8">
        <div class="row">
          <div class="col s12 m3 footer-section">
            <div class="footer-section-title">Company</div>
            <ul>
              <li><a href="/about">About Us</a></li>
              <li><a href="/about">The Brands</a></li>
              <li><a href="/about">Contact Us</a></li>
            </ul>
          </div>
          <div class="col s12 m3">
            <div class="footer-section-title">Support</div>
            <ul>
               <li><a href="/help">Help Center</a></li>
               <li><a href="/help/docs/faq/">FAQ</a></li>
              <li><a href="mailto:help@gaggle.email">Contact Support</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="container" id="footer-footer">
        <span class="right">Made with <i class="materialize-red-text text-lighten-3 mdi-action-favorite">heart</i> in Philippines</span>
        <div>© 2019 Copyright</div>
    </div>
  </div>
</footer>