<?php if($include) { ?>

    <div class="loader">
        <div class="progress" style="margin: 0">
            <div class="indeterminate"></div>
        </div>
    </div>

<?php } else { die; } ?>