<?php if($include) { ?>

  <ul id="slide-out" class="sidenav">
    <li>
      <div class="user-view">
        <div class="background">
          <img src="assets/temp.jpg">
        </div>
      </div>
    </li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Company</a></li>
    <li><a class="waves-effect" href="index.php"><i class="material-icons">home</i>Home</a></li>
    <li><a class="waves-effect" href="about-us.php"><i class="material-icons">info_outline</i>About Us</a></li>
    <li><a class="waves-effect" href="index.php#contactus"><i class="material-icons">headset_mic</i>Contact Us</a></li>
    <li><a class="waves-effect" href="#!"><i class="material-icons">menu</i>Our Brands</a></li>
    <ul class="collection">
      <li class="collection-item"><div>brand 1<a href="#!" class="secondary-content"><i class="material-icons">chevron_right</i></a></div></li>
      <li class="collection-item"><div>brand 2<a href="#!" class="secondary-content"><i class="material-icons">chevron_right</i></a></div></li>
    </ul>
  </ul>
<div class="navbar clear" id="myHeader">
    <p class="float-left brand cl-white"><span class="bold"><a href="index.php">Senz</span> Medical</a></p>

    <p class="float-right navi"><a href="#" data-target="slide-out" class="sidenav-trigger"><span id="pc" class="align-ico-nav">Brands </span><i class="material-icons">menu</i></a>
    </p>
    
</div>

<?php } else { die; } ?>