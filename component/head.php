<?php if($include) { ?>
<meta name="Resource-type" content="Document" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="format-detection" content="telephone=no" />
<meta name="google" content="notranslate" />
<!-- <link rel="shortcut icon" href="static/favicon.png" type="image/x-icon"> -->




<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700&display=swap" rel="stylesheet" />
<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" /> -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
<link href="static/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
<link rel="stylesheet" type="text/css" href="static/keyframes.css" />
<link rel="stylesheet" type="text/css" href="static/main.css" />
<link rel="stylesheet" type="text/css" href="static/theme.css" />
<link rel="stylesheet" type="text/css" href="static/navbar.css" />
<link rel="stylesheet" type="text/css" href="static/responsive.css" />





<script src="static/jquery.min.js"></script>
<script src="static/materialize.js"></script>
<script src="static/main.js"></script>

<?php } else { echo "Forbidden!";}?>